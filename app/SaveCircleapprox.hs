module Save where

--import Graphics.Gloss.Data.Picture
import Graphics.Gloss.Data.Color
import Graphics.Gloss.Export
import Lib



animation = circleapproxAnimation . (*0.25)

main :: IO ()
main = exportPicturesToGif 40 LoopingNever (900,450) white "polar_animation_circleapprox.gif" animation [0,1..198]
--LoopingForever
