module Save where

--import Graphics.Gloss.Data.Picture
import Graphics.Gloss.Data.Color
import Graphics.Gloss.Export
import Lib (circleTourAnimation)



animation = circleTourAnimation . (*0.25)

main :: IO ()
main = exportPicturesToGif 40 LoopingNever (900,450) white "polar_animation_circletour.gif" animation [0,1..340]
--LoopingForever
