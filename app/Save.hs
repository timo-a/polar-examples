module Save where

import Graphics.Gloss.Data.Color
import Graphics.Gloss.Export
import Lib

animation = wholePicture . (*0.5)

main :: IO ()
main = do
  -- my machine can't produce the whole gif in one go -> gifsicle polar_animation_?.gif > polar_animation_full.gif
  part 0 range
  part 1 $ map (+100) range
  part 2 $ map (+200) range
  part 3 $ map (+300) range
  part 4 $ map (+400) range
  part 5 $ map (+500) range
  part 6 $ map (+600) range
  part 7 $ map (+700) $ take 126 range
    where
       range = [  0..199]
       part :: Int -> [Float] -> IO ()
       part i range = exportPicturesToGif 20           -- centiseconds between frames
                                    LoopingNever -- one time no loop
                                    (1000,700)   -- (w,h)
                                    white        -- bg
                                    ("polar_animation_" ++ show i ++ ".gif") -- save as
                                    animation
                                    range -- time steps (implicitly time resolution)
