module Save where

--import Graphics.Gloss.Data.Picture
import Graphics.Gloss.Data.Color
import Graphics.Gloss.Export
import Lib



animation = niklaushausAnimation . (*0.25)

main :: IO ()
main = do
    exportPicturesToGif 40 LoopingNever (900,450) white "polar_animation_niklaushaus.gif" animation [0,1..368]
--LoopingForever
