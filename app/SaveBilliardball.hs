module Save where

--import Graphics.Gloss.Data.Picture
import Graphics.Gloss.Data.Color
import Graphics.Gloss.Export
import Lib



animation = billiardballAnimation . (*0.25)

main :: IO ()
main = do
    exportPicturesToGif 40 LoopingNever (900,450) white "polar_animation_billiard.gif" animation [0,1..39]
--LoopingForever
