module Main where

import Graphics.Gloss
import Lib

main :: IO ()
main = animate (InWindow "Polar Mapping" (900,450) (80, 80)) -- name, size, position
                         (makeColor 0.999 0.999 0.999 1)
                         (wholePicture . (*0.5))
