# polar-examples

Examples of how to create gifs from gloss animations with [gloss-export](https://hackage.haskell.org/package/gloss-export-0.1.0.2).

On the left side we see familiar shapes in a cartesian coordinate system with angles already indicated. On the left side the same shape but in polar coordinates with *radius* as *Y-axis* and *angle* as *X-Axis*.



![circle](https://gitlab.com/timo-a/polar-examples/raw/master/gifs/polar_animation_circletour.gif)
![rectangle](https://gitlab.com/timo-a/polar-examples/raw/master/gifs/polar_animation_rectangletour.gif)
![circle](https://gitlab.com/timo-a/polar-examples/raw/master/gifs/polar_animation_circleapprox.gif)
![circle](https://gitlab.com/timo-a/polar-examples/raw/master/gifs/polar_animation_circleapproxshift.gif)
![circle](https://gitlab.com/timo-a/polar-examples/raw/master/gifs/polar_animation_bounciball.gif)
![circle](https://gitlab.com/timo-a/polar-examples/raw/master/gifs/polar_animation_tetris.gif)
![circle](https://gitlab.com/timo-a/polar-examples/raw/master/gifs/polar_animation_billiard.gif)
![circle](https://gitlab.com/timo-a/polar-examples/raw/master/gifs/polar_animation_niklaushaus.gif)
