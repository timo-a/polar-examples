module Animations.BounciBall
  ( bounciball
  ) where

import Graphics.Gloss
import Listify
import PictureExtentionsCopy

----- (incomplete) bounciball (ball drops in from left bounces on y=0 (bottom line as well?) goes out on right side again) --
bounciball :: AnimationShort
bounciball = Node  [Leaf 20 allBalls2]

allBalls :: Float -> Float -> Picture
allBalls d t = Pictures (elevenBalls ++ [(translate 0 (-8) . scale' 0.01 . Text . show) t])
            where
              elevenBalls = zipWith (\g b -> Color (greyN g) (ballPos b)) shades times
              times = reverse  $(take 11 . map (max 0) . iterate (\a -> a-epsilon)) t --t, t-e, t-2e, ...0
              epsilon :: Float
              epsilon = 0.1
              shades = [1.0,0.9..0.1] -- from white to black


allBalls2 :: Float -> Float -> Picture
allBalls2 d t = Pictures (trail ++ [Color red (ballPos t)])
            where
              trail = take (floor (t / diff) +1) ballstorage

{-
e.g.  t = 3.5 = 17*0.2 + 0.1
-}
--todo use solid circle instead of circle

ballstorage :: [Picture]
ballstorage = zipWith (\c t -> (Color c ( ballPos (t * diff))) ) (cycle [black,blue]) [0..]
     
diff = 0.1   -- as we can't save every ball we save in steps of 0.2
       
ballPos :: Float -> Picture
ballPos t = translate (t'-20) (ballposY t' + r) (circle r)
 where
   r=0.6
   t' = t*2 -- higher factor -> higher speed. does not change trajectory

ballposY :: Float -> Float
ballposY t | t < 3     = 2*(9 - t^2)
           | otherwise = helper (t-3) (3*0.9) 2
           where
             helper t xlim a | t <= 2*xlim = a * (xlim^2 - (t-xlim)^2)
                             | otherwise   = helper (t-2*xlim) (xlim*0.9) a
