module Animations.NGonToCircleAndBack
  ( ngon2oo
  , ngon2ooTransition
  , ngon2ooShifted
  ) where

import Graphics.Gloss
import Listify
import Graphics.Gloss.Geometry.Angle
import Graphics.Gloss.Geometry.Line
--import PictureExtentionsCopy
import Graphics.Gloss.Polarizer

-- n-hedron with n -> oo   maybe same r(distance origin - corner) up to oo and back down to gon with same h(origin - edge midpoint)

ngon2ooShifted :: AnimationShort
ngon2ooShifted = (Translate 5 5) `amap` ngon2oo

ngon2ooTransition :: AnimationShort
ngon2ooTransition = Leaf 2 (\d t -> Translate (5 * i d t) (5 * i d t) triangle)
 where
   i d t = t/d
   radius = 10
   triangle        = Line [ (2 * radius  ,         0)
                          , (-radius,   sqrt 3 * radius)
                          , (-radius, - sqrt 3 * radius)
                          , (2 * radius  ,         0)
                          ]
   

ngon2oo :: AnimationShort
ngon2oo = Node (grow ++ reverse shrink ++ lastFrame ++ [Leaf 0.1 (\_ _ -> Blank)])
          where
            radius = 10
            grow  = zipWith (\d k ->  Leaf d (\d t -> k_to_kp1_gon       radius k (    t/d) )) durations nrVertices
            shrink= zipWith (\d k ->  Leaf d (\d t -> k_to_kp1_gon_inner radius k (1 - t/d) )) durations nrVertices
            nrVertices = [3..25]
            durations= 4:3:3:2:2:1:1:0.8:[0.4,0.4..] -- durations get shorter

            lastFrame = [Leaf 4 (\_ _ -> Pictures [ circleIndicator
                                                    , triangle
                                                    ])]
                     where
                          circleIndicator = Color (greyN 0.8) $ Circle radius
                          triangle        = Line [ (2 * radius  ,         0)
                                                 , (-radius,   sqrt 3 * radius)
                                                 , (-radius, - sqrt 3 * radius)
                                                 , (2 * radius  ,         0)
                                                 ]
                          
--TODO translate to 0 5, see what happens then
--TODO transition

-- |
k_to_kp1_gon :: Float -- radius of the inscribed circle
             -> Int   -- number of vertices in the original n-gon
             -> Float -- ∈ [0:1] progress of interpolation, 0 being original k-gon, 1 being (k+1)-gon
             -> Picture
k_to_kp1_gon radius k interpolation = Pictures [ circleIndicator -- circle
                                               , n_gon_smooth    -- n-gon aproxxomation of a circle
                                               ]
    where
     circleIndicator = Color (greyN 0.8) $ Circle radius
     n_gon_smooth = Line (if odd k then oddLine else evenLine)
            
     oddLine  = map toCartesian $ loop $ zip anglesCore [radius,radius..]
              where
                loop p = last p : p
     evenLine = fixEnd $ zipWith (curry toCartesian) (loop anglesCore) [radius,radius..]
              where
                loop p = (head p - mark ): p ++ [last p + mark]
                fixEnd (p:ps) = yZero p : init ps ++ [yZero (last ps)] -- funktioniert aber sinusförmig da wir bei einer kreisbahn y nullsetzen
                                                                        -- mit y=0 schneiden wär's linear
     anglesCore = _0to180 ++ _180to360
           where
             _0to180   = reverse (take m (iterate (\x->x-mark) (180-0.5*mark)))
             _180to360 =          take m (iterate (+mark)      (180+0.5*mark))
             m=div (k+1) 2
                
     --n_gon_natural = Line $ map toCartesian $ zip angles [radius,radius..]     
     angles = _0to180 ++ _180to360
           where
             _0to180   = reverse (take m (iterate (\x->x-mark) (180-0.5*mark)))
             _180to360 =          take m (iterate (+mark)      (180+0.5*mark))        
             m=1+div (k+1) 2
     mark  = 360/(fromIntegral k + interpolation)
     yZero (a,_) = (a,0)

     
k_to_kp1_gon_inner height k interpolation = Pictures [circleIndicator, n_gon_smooth]
    where
     radius = height / cos alpha --degToRad 180))
     alpha = degToRad (0.5*mark)
     mark  = 360/(fromIntegral k + interpolation)
     
     n_gon_smooth = Line (if odd k then oddLine else evenLine)
     circleIndicator = Color (greyN 0.8) $ Circle height
     
     evenLine = fixEnd $ zipWith (curry toCartesian) (loop anglesCore) [radius,radius..]
              where
                loop p = (head p - mark ): p ++ [last p + mark]
                fixEnd (h0:h1:ps) = intersectP : h1:init ps ++ [intersectP]
                  where
                    intersectP = case intersectLineLine (0,0) (1,0) h0 h1 of (Just point) -> point
                                                                             Nothing      -> h1
               
     oddLine  = fixEnd $ zipWith (curry toCartesian) (loop anglesCore) [radius,radius..]
              where
                loop p = (head p - mark): p
                fixEnd (h0:h1:hs) = p: h1:init hs ++ [p]
                  where
                    l1 = last hs
                    l2 = last (init hs)
                    p = case intersectLineLine l1 l2 h0 h1 of (Just point) -> point
                                                              Nothing      -> h1

     anglesCore = reverse (take m (iterate (\x->x-mark) (180-0.5*mark))) ++ take m ( iterate (+mark) (180+0.5*mark))
           where
             m=div (k+1) 2
              
     -- todo test this or find an interpretation for this
     -- n_gon_natural = Line $ map toCartesian $ zip angles [radius,radius..]              
     --angles = (reverse (take m (iterate (\x->x-mark) (180-0.5*mark)))) ++ (take m ( iterate (+mark) (180+0.5*mark)))
     --      where
     --        m=1+div (k+1) 2



--------------- I don't think they are used...
-- transforms height (distance to edge) into radius (dist to corner and back) k for kgon, interpolation not yet figured out
--h2r k h = h / cos (pi / fromIntegral k)
--r2h k r = r * cos (pi / fromIntegral k)


----------------------------------------------------------------------
--n_gon :: Float -> Int -> Picture
--n_gon r n = Line $ zipWith (curry toCartesian) (init [0,mark..360]) [r..]
--         where
--           mark = 360 / fromIntegral n

----------------------------------------------------------------------
