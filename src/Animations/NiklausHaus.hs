module Animations.NiklausHaus (niklaushaus) where

import Graphics.Gloss

import Listify
import PictureExtentionsCopy (papply, interpolateLast2)


niklaushaus :: (Float -> Float -> Float -> Float) -> AnimationShort
niklaushaus lin = mapApply (color blue) $ Node (draw++move++undraw)
     where
       draw   = map step [2..9]
           where
             step s = Leaf 5 (\d t -> papply (interpolateLast2 (t/d) . take s) nikLine)

       move   = [Leaf 5 (\d t -> translate 0 (lin d 5 t) nikLine)
                ,Leaf 1 (\_ _ -> translate 0 5 nikLine)
                ,Leaf 5 (\d t -> translate (lin d 5 t) 5 nikLine)
                ,Leaf 1 (\_ _ -> translate 5 5 nikLine)
                ]
       undraw = map step [9,8..2]
           where
             step s = Leaf 5 (\d t -> translate 5 5 $ papply (interpolateLast2 (1-(t/d)) . take s . reverse) nikLine)


mapApply :: (Picture -> Picture) -> AnimationShort -> AnimationShort
mapApply p2p (Node anims) = Node (map (mapApply p2p) anims)
--mapApply p2p (A d f) = A d (\d t -> p2p (f d t))
mapApply p2p (Leaf d f) = Leaf d (p2p ... f)

(...) = (.) . (.) -- blackbird ~ dot for initial 2 args -> (b -> c) -> (a1 -> a2 -> b) -> a1 -> a2 -> c



nikLine =  Line [(-5,-5),( 5,-5),(-5, 5),( 5, 5), (-5,-5), (-5, 5), (0,10), (5,5), (5,-5)]

niklaushaus' = Leaf 15 (\d t -> Line  [(-15,-15),(-5, 5),( 1, 1),(-1,-1)]) 
--niklaushaus = A 5 (\d t -> Line (interpolateLast2 0 [(-15,-15),(-5, 5),( 1, 1),(-1,-1), (-5,-5)]) )
