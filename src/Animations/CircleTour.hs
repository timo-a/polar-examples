module Animations.CircleTour
  ( circleAnimationShort
  ) where

import Graphics.Gloss
import Listify
import PictureExtentionsCopy

-- circle grows , stretches, revolves around origin --
circleAnimationShort :: AnimationShort
circleAnimationShort = Node  [Leaf  5 circleGrow        
                             ,Leaf  2 (\_ _ -> circle 5)
                             ,Leaf  8 circleUpDown
                             ,Leaf 10 circleUpHigh
                             ,Leaf 25 circleRevolution
                             ,Leaf 30 circleElipsis
                             ,Leaf  5 circleShrinkCentered
                             ,Leaf  2 (\_ _ -> Blank)
                            ]--67

--circle in origin grows to r=5
circleGrow :: Float -> Float -> Picture
circleGrow duration = circle . (*5) . sin . f duration 0.5

--circle smoothly goes up to 0,6, down to 0,-6, up to 0,6 again
circleUpDown :: Float -> Float -> Picture
circleUpDown duration t | t < duration*0.2 = translate 0 (6* (0.5-0.5*cos ( f (0.2*duration) 1  t              )))  $ circle 5
                        | otherwise        = translate 0 (6*          cos ( f (0.4*duration) 1 (t-duration*0.2)) )  $ circle 5


-- given duration in s and way in pi, scales current t
f :: Float -> Float -> Float -> Float
f duration timesPi =  (pi * (timesPi / duration) *)



--circle smoothly goes from 0,6 to 0,14 
circleUpHigh :: Float -> Float -> Picture
circleUpHigh duration t = translate 0 (6+ 8*(0.5-0.5*cos ( f duration 1  t ))) $ circle 5

circleRevolution :: Float -> Float -> Picture
circleRevolution duration t | t < (duration*0.25) =                 translate (t*a)      (14 - t*a) $ circle 5
                            | t < (duration*0.75) = rotate (t'*b) $ translate  14          0        $ circle 5
                            | otherwise           =                 translate (t''*a -14) (t''*a)   $ circle 5
                            where
                              a=14/(duration*0.25)
                              b=180/(duration*0.5)
                              t' = t- duration*0.25
                              t''= t- duration*0.75

circleElipsis :: Float -> Float -> Picture
circleElipsis duration t | t < (duration*0.2) = translate 0 14 $ scale (cos' t 0) 1    $  circle 5
                         | t < (duration*0.4) = translate 0 14 $ scale  1  (cos' t 1)  $  circle 5
                         | t < (duration*0.6) =                    translate 0 (14-t') $ circle 5
                         | t < (duration*0.8) = scale  (cos' t 3) 1   $ circle 5
                         | otherwise          = scale  1  (cos' t 4)  $ circle 5
                         where
                           cos' t i = cos (f (duration*0.2) 2 t')
                                    where t' = t-duration*0.2*i
                           t'= (t-duration*0.4) * 14/(duration*0.2)
                           
                      
--circle goes from 0,6 back to center and simultaneously shrinks to r=0
circleShrinkCentered :: Float -> Float -> Picture
circleShrinkCentered duration t = circle (5-t*5/duration)

