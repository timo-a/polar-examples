module Animations.BilliardBall
  ( billiardBall
  ) where

import Graphics.Gloss
import Listify
import PictureExtentionsCopy

--Ball goes billiard style in from right, hits top, hits right, hits bottom, goes out left again

billiardBall :: AnimationShort
billiardBall = Node  [Leaf 10 (\d t -> papply (interpolatePath (t/d)) billiardLine)] --interpolate needs the par of the line before, not just 1 point!

billiardLine =  Line [(-20, 12)
                     ,(-10, 20)
                     ,( 20, -4)
                     ,(  0,-20)
                     ,(-20, -4)]
