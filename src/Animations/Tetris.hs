module Animations.Tetris
  ( tetris
  , tetrisDeb
  ) where

import Graphics.Gloss
import Listify
import PictureExtentionsCopy

----- tetris ------
--tetris = Leaf  7 (\d t -> scale' 4 $ tr (-1) 0 (color red $ Line    [(0,0),(0.9,0.1),(1,1),(0,1)] ))
--tetris = Leaf 70 (\d t -> scale' 4 $ tr (-1) 0 (color red $ Polygon [(0,0),(1,0),(1,1),(0,1)]     ))

--tetrisDeb = Leaf 20  (\d t -> scale' 4 $                   color green $ Polygon [( 0,0),(1,0),(1,1),( 0,1)])
tetrisDeb = Leaf 40  (\d t -> scale' 4 $ tr 1 2 $ ro 180 $ color green $ Polygon [( 0,1),(1,1),(1,2),( 0,2)])

tetris = Node [fall4x1, fallL, fallZigZag, fallHat, deleteRow]

mapRest :: [Picture] -> AnimationShort -> AnimationShort
mapRest already (Node anims) = Node (map addToAnim anims)
   where
     addToAnim :: AnimationShort -> AnimationShort
     addToAnim (Leaf d f) = Leaf d (\d t -> addRest already (f d t) )
     addRest :: [Picture] -> Picture -> Picture
     addRest already (Scale x y pic) = Scale x y (Pictures (pic:already))


fall4x1  = Node (            Leaf 2   (\d t -> scale' 4 $ tr (-2) 4 draw4x1)
                : map (\x -> Leaf 0.5 (\d t -> scale' 4 $ tr x 3 draw4x1)) [(-2),(-1)..1]
               ++ map (\y -> Leaf 2   (\d t -> scale' 4 $ tr 1 y draw4x1)) [2,1,0])
           
fallL = mapRest already $ Node [Leaf 0.5 (\d t -> scale' 4 $ tr (-1) 2             drawL) -- |
                                ,Leaf 0.5 (\d t -> scale' 4 $ tr    2 2 $ ro  (-90) drawL) -- | step one rotate
                                ,Leaf 0.5 (\d t -> scale' 4 $ tr    2 5 $ ro (-180) drawL) -- |
                                ,Leaf 0.5 (\d t -> scale' 4 $ tr (-1) 5 $ ro    90  drawL) -- L 2s
                                ,Leaf 0.4 (\d t -> scale' 4 $ tr (-1) 4 $ ro    90  drawL) --
                                ,Leaf 0.4 (\d t -> scale' 4 $ tr (-2) 4 $ ro    90  drawL) -- |
                                ,Leaf 0.4 (\d t -> scale' 4 $ tr (-3) 4 $ ro    90  drawL) -- | step two shift left
                                ,Leaf 0.4 (\d t -> scale' 4 $ tr (-4) 4 $ ro    90  drawL) -- |
                                ,Leaf 0.4 (\d t -> scale' 4 $ tr (-5) 4 $ ro    90  drawL) -- L 2s
                                ,Leaf 2   (\d t -> scale' 4 $ tr (-5) 3 $ ro    90  drawL)
                                ,Leaf 2   (\d t -> scale' 4 $ tr (-5) 2 $ ro    90  drawL)
                               ]
           where               
             already = [tr 1 0 draw4x1]

fallZigZag = mapRest already $ Node [Leaf 2   (\d t -> scale' 4 $ tr    0 4 drawZigZag)
                                    ,Leaf 0.2 (\d t -> scale' 4 $ tr    0 3 drawZigZag)
                                    ,Leaf 0.9 (\d t -> scale' 4 $ tr (-1) 3 drawZigZag)
                                    ,Leaf 0.9 (\d t -> scale' 4 $ tr (-2) 3 drawZigZag)
                                    ,Leaf 2   (\d t -> scale' 4 $ tr (-2) 2 drawZigZag)
                                    ,Leaf 2   (\d t -> scale' 4 $ tr (-2) 1 drawZigZag)
                                    ]
             where
              already = [tr (-5) 2 $ ro 90 drawL, tr 1 0 draw4x1]


fallHat = mapRest already $ Node  [Leaf 2   (\d t -> scale' 4 $ tr 0 3          drawHat)
                                  ,Leaf 0.1 (\d t -> scale' 4 $ tr 0 2          drawHat)
                                  ,Leaf 0.2 (\d t -> scale' 4 $ tr 0 3 $ ro  90 drawHat)
                                  ,Leaf 0.2 (\d t -> scale' 4 $ tr 1 3 $ ro 180 drawHat)
                                  ,Leaf 0.2 (\d t -> scale' 4 $ tr 1 2 $ ro 270 drawHat)
                                  ,Leaf 0.3 (\d t -> scale' 4 $ tr 1 3 $ ro 180 drawHat)
                                  ,Leaf 2   (\d t -> scale' 4 $ tr 1 2 $ ro 180 drawHat)
                                 ]
           where
              already = [tr (-2) 1 drawZigZag, tr (-5) 2 $ ro 90 drawL, tr 1 0 draw4x1]

deleteRow = Node  [Leaf 0.5 (\d t -> scale' 4 $ Pictures full)
                  ,Leaf 0.5 (\d t -> scale' 4 $ Pictures top)
                  ,Leaf 0.5 (\d t -> scale' 4 $ Pictures full)
                  ,Leaf 1   (\d t -> scale' 4 $ Pictures top)
                  ,Leaf 1   (\d t -> scale' 4 $ translate 0 (-1) $ Pictures top)
                 ]
            where
              full = [tr    1 2 $ ro 180 drawHat
                     ,tr (-2) 1 drawZigZag
                     ,tr (-5) 2 $ ro 90 drawL
                     ,tr 1 0 draw4x1
                     ]
              top  = [color blue $ Polygon [(-5,1),(-4,1),(-4,2),(-5,2)] --bottom line removed
                     ,color cyan $ Polygon [(-3,1),(-1,1),(-1,2),(-3,2)]
                     ,color green$ Polygon [(-1,1),( 2,1),( 2,2),(-1,2)]
                     ]

tr = translate
ro = rotate
         
draw4x1 = color red $ Polygon [(0,0),(4,0),(4,1),(0,1)]
draw2x2 = Line [(0,0),(2,0),(2,2),(0,2)]

drawL = color blue $ Pictures [Polygon [(2,3),(2,1),(1,1),(1,3)]  -- {- #
                              ,Polygon [(0,0),(0,1),(2,1),(2,0)]  --    #
                              ]                                   --   ## -}

drawZigZag = color cyan $ Pictures [Polygon [(-1,0),(1,0),(1,1),(-1,1)]
                                   ,Polygon [(0,0),(2,0),(2,-1),(0,-1)]
                                   ]

drawHat = color green $ Pictures [Polygon [(-1,0),(0,0),(0,1),(-1,1)]
                                 ,Polygon [( 0,0),(1,0),(1,1),( 0,1)]
                                 ,Polygon [( 1,0),(2,0),(2,1),( 1,1)]
                                 ,Polygon [( 0,1),(1,1),(1,2),( 0,2)]
                                 ]
