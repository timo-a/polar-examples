module Animations.RectangleTour
  ( rectangleTourShort
  ) where

import Graphics.Gloss
import Listify
import PictureExtentionsCopy

rectangleTourShort :: (Float -> Float -> Float -> Float) -> AnimationShort
rectangleTourShort lin = Node  [Leaf  5 (\duration t -> rectangle (lin duration 5 t))
       {-    ↑   -}       ,Leaf  5 (\duration t -> translate 0 (lin duration l t)  rect5)
       {-     -> -}       ,Leaf  5 (\duration t -> translate (lin duration l t) l  rect5)
       {-      ↓ -}       ,Leaf 10 (\duration t -> translate l (l - lin duration (2*l) t)    rect5)
       {-  <---- -}       ,Leaf 10 (\duration t -> translate (l - lin duration (2*l) t) (-l) rect5)
       {-  ↑     -}       ,Leaf 10 (\duration t -> translate (-l) (-l + lin duration (2*l) t)  rect5)
       {-  ->    -}       ,Leaf  5 (\duration t -> translate (-l + lin duration l t) l   rect5)
       {-   ↷    -}       ,Leaf  5 (\duration t -> translate 0 l $ rotate (lin duration 180 t) rect5)
                          ,Leaf  5 (\duration t -> translate 0 (l- lin duration l t) rect5)
                          ,Leaf  5 (\duration t -> rotate (lin duration 180 t) rect5)
                          ,Leaf  5 (\duration t -> rectangle (5 - lin duration 5 t))
                         ]
                    where l = 12.5

rect5 = rectangle 5

rectangle :: Float -> Picture
rectangle h = lineLoop [(-h,h), (h,h), (h,-h),(-h,-h)]
