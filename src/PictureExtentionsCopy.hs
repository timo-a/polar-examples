module PictureExtentionsCopy
    ( scale'
    , papply
    , interpolateLast2
    , interpolatePath
    ) where

import Graphics.Gloss
import Graphics.Gloss.Data.Vector
--import Graphics.Gloss.Geometry.Angle

scale' :: Float -> Picture -> Picture
scale' x = scale x x

papply :: (Path -> Path) -> Picture -> Picture
papply f (Line p)     = Line (f p)
papply f (Polygon p)  = Polygon (f p)
papply f (Pictures p) = Pictures (map (papply f) p)
papply _ pic          = pic


(.-.) :: Point -> Point -> Point
(.-.)  (a,b) (c,d) = (a-c, b-d)
  
(.+.) :: Point -> Point -> Point
(.+.)  (a,b) (c,d) = (a+c, b+d)

distance :: Point -> Point -> Float                        
distance a b = dotV d d where d=a .-. b
                                    
serialize :: Num a => [a] -> [a]
serialize = scanl1 (+) -- turn seq to series                       

interpolate :: Float -> Point -> Point -> Point
interpolate i a b = a .+. mulSV i (b .-. a)   

interpolateLast2 :: Float -> [Point] -> [Point]    --interpolate last 2 points with i
interpolateLast2 i list = init list ++ [interpolate i a b]
  where
    a = last (init list)
    b = last list

interpolatePath :: Float -> [Point] -> [Point]
interpolatePath i list = uncurry interpolateLast2 (getPoints [] i interPoints list)
  where
    distances = 0:serialize (zipWith distance list (tail list)) :: [Float]
    interPoints = map (/last distances) distances :: [Float]
    -- normalize i s.t. a becomes 0, b becomes 1
    normalizeI i a b = (i-a)/(b-a)
    getPoints :: [Point] -> Float -> [Float] -> [Point] -> (Float, [Point])
    getPoints acc i (ip1:ip2:ips) (l1:l2:ls) | i < ip2   = (i', acc++[l1,l2])
                                             | otherwise = getPoints (acc++[l1]) i (ip2:ips) (l2:ls)
                                             where
                                               i' = normalizeI i ip1 ip2
