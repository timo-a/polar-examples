module Lib
    ( wholePicture
    , circleTourAnimation
    , rectangleTourAnimation
    , circleapproxAnimation
    , circleapproxshiftedAnimation
    , bounciballAnimation
    , tetrisAnimation
    , billiardballAnimation
    , niklaushausAnimation
    ) where

import Graphics.Gloss
import Graphics.Gloss.Polarizer
import CartesianAnimation (cartesianPic, circleAnimationShort, justRectangleTour, justCircleapprox, justCircleapproxShifted, justBounciball, justTetris, justBilliardball, justNiklaushaus)
import PictureExtentionsCopy (scale')
import Listify

wholePicture :: Float    -- ^ time value t
             -> Picture  -- ^ picture at time t
wholePicture = framePic cartesianPic 

animationShort2Frame :: AnimationShort -> Float -> Picture
animationShort2Frame as = framePic $ feed (listify as) 

circleTourAnimation :: Float -> Picture
circleTourAnimation =  animationShort2Frame circleAnimationShort

rectangleTourAnimation :: Float -> Picture
rectangleTourAnimation = framePic justRectangleTour

circleapproxshiftedAnimation :: Float -> Picture
circleapproxshiftedAnimation = framePic justCircleapproxShifted
  
circleapproxAnimation :: Float -> Picture
circleapproxAnimation = framePic justCircleapprox

bounciballAnimation :: Float -> Picture
bounciballAnimation = framePic justBounciball
  
tetrisAnimation :: Float -> Picture 
tetrisAnimation = framePic justTetris

billiardballAnimation :: Float -> Picture 
billiardballAnimation = framePic justBilliardball

niklaushausAnimation :: Float -> Picture
niklaushausAnimation = framePic justNiklaushaus

-- |'wholePicture' draws the whole 'Picture': the cartesian original with
--                                                   - frame
--                                                   - label "Cartesian Coordinates"
--                                                   - angle indicators
--                                                   - axes
-- and the cartesian picture with all points as seen from a polar perspective with
--                                                   - frame
--                                                   - label "Polar Coordinates"
--                                                   - angles as ticks on horizontal axis
--                                                   - axes
framePic :: (Float -> Picture)
         -> Float    -- ^ time value t
         -> Picture  -- ^ picture at time t
framePic cartesianPic t = Pictures (shiftLeft cartesian ++ shiftRight polar)
                 where
                   l = 20
                   cartesian :: [Picture]
                   cartesian = map (scale' 9) [cp, frame, cAxes, ctext]
                      where
                         ctext = Pictures [translate (-l)  (l+0.4) $ scale' 0.01 $ Text  "Cartesian Coordinates"
                                          ,translate (l+0.4)   (-0.5) $ scale' 0.01 $ degreeText   "0"  --right
                                          ,translate (-0.73)  (l+0.4) $ scale' 0.01 $ degreeText  "90"  --top
                                          ,translate (-l-2.9)  (-0.5) $ scale' 0.01 $ degreeText "180"  --left
                                          ,translate (-1.2)  (-l-1.3) $ scale' 0.01 $ degreeText "270"  --bottom
                                          ]

                         cAxes  = scale' l $ Pictures  [Line [(0,1),(0,-1)], Line [(-1,0),(1,0)]]
                   frame = lineLoop $ rectanglePath (l+l) (l+l)
                   cp   = cartesianPic t                        
                   polar :: [Picture]
                   polar     = map (scale' 9) [ pp, frame, ptext]
                       where
                         ptext = Pictures [translate (-l)    (l+0.4) $ scale' 0.01 $ Text  "Polar Coordinates"
                                          ,translate (-l)   (-l-1.3) $ scale' 0.01 $ degreeText "0"
                                          ,translate (-l/2) (-l-1.3) $ scale' 0.01 $ degreeText "90"
                                          ,translate  0     (-l-1.3) $ scale' 0.01 $ degreeText "180"
                                          ]

                         -- transforms cartesian 'Picture' to polar then transforms it to fit the frame 
                         format2polar = translate (-l) (-l) . scale (l/180) (l/polarHeight) . polarPic 
                         pp = format2polar cp
                         polarHeight = 10
                   shiftLeft :: [Picture] -> [Picture]
                   shiftLeft  =  shift (-210)
                   shiftRight:: [Picture] -> [Picture]
                   shiftRight = shift 210
                   shift x = map (translate x 0)


-- `text 0` produces a 0 in the ractangle `lineLoop [(4.98,0), (71.6467,0),  (71.6467,100),(4.98,100)]`

-- |gloss can only display ascii but ° is outside ascii -> draw it
degreeText :: String  -- ^ Number
           -> Picture -- ^ the number from input but with `°` behind it
degreeText t | t ==   "0" = addDegreeSign   "0"        0
             | t ==  "90" = addDegreeSign  "90"  74.0648
             | t == "180" = addDegreeSign "180" 144.1962
             | t == "270" = addDegreeSign "270" 154.8734
             | t == "360" = addDegreeSign "360" 150.8515
             | otherwise = error "degreeText is only defined for the strings 0,90,180,270,360 for now" -- one could do mean but I'd rather throw error here
             where
               offset = 20 + 86 -- not sure why this offset was chosen...
               
               addDegreeSign :: String -> Float -> Picture
               addDegreeSign deg xshift = Pictures [Text   deg, Translate (xshift+offset) 88 $ Circle 12]
