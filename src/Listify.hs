module Listify
    ( feed
    , listify
    , amap
    , Animation(..) -- export all constructors
    , AnimationShort(..)
    ) where

--import GHC.Float
import Graphics.Gloss

-- |convenient format for notation, a tree structure
-- An AnimationShort consists of either a list of AnimationShort
--                               or a duration and a function turning duration and time into a picture     
-- this format is easy to read for the animation designer
data AnimationShort = Node [AnimationShort]
                    | Leaf Float (Float -> Float -> Picture) -- A duration (duration -> time -> pic)



-- no functor because AnimationShort is not of kind * -> * and there's no reason to generalize
-- recursively chains a function to the existing ones
amap :: (Picture -> Picture) -> AnimationShort -> AnimationShort
amap f (Node anlist)     = Node (map (amap f) anlist)  -- mapp to every element
amap f (Leaf duration g) = Leaf duration (f ... g)     -- chain `f` to `g`
                             where
                               (...) = (.) . (.)
                    
-- this format is easy to read for the programm
data Animation = Anim Float -- ^ threshold i.e. absolute starting time
                      Float -- ^ duration
                      (Float -> Float -> Picture)  -- ^ function f

instance Show Animation where
  show (Anim a b _) = "Anim " ++ show a ++ " " ++ show b ++ " f"

-- from the list of animations and current timestep generate current picture
feed :: [Animation] -> Float -> Picture
feed [] _ = Blank
feed (Anim thresh duration animation:as) t = if t < thresh+duration  then animation duration (t-thresh) -- found slot, compute picture
                                                                     else feed as t                     -- search further
                                                                          
--turn animation short tree into list of animations
listify :: AnimationShort -> [Animation]
listify tree = fst $ l tree 0 
  where
    l :: AnimationShort -> Float -> ([Animation], Float)
    l (Leaf d f)     acc = ([Anim acc d f], acc+d)
    l (Node list) acc = foo acc [] list
                      where foo :: Float -> [Animation] -> [AnimationShort] -> ([Animation], Float) -- 
                            foo acc accL []         = (accL,acc)
                            foo acc accL (anims:as) = foo acc' (accL++li) as
                                                    where
                                                      (li, acc') = l anims acc
