module CartesianAnimation
    ( cartesianPic
    , circleAnimationShort
    , justRectangleTour
    , justCircleapprox
    , justCircleapproxShifted
    , justBounciball
    , justTetris
    , justBilliardball
    , justNiklaushaus
    , scale'
    , animationList
    ) where

import Graphics.Gloss
import Graphics.Gloss.Geometry.Angle
import Listify
import PictureExtentionsCopy
import Animations.CircleTour (circleAnimationShort)
import Animations.RectangleTour (rectangleTourShort)
import Animations.NGonToCircleAndBack (ngon2oo, ngon2ooShifted)
import Animations.BounciBall
import Animations.Tetris (tetris)
import Animations.BilliardBall (billiardBall)
import Animations.NiklausHaus (niklaushaus)

cartesianPic :: Float -> Picture
cartesianPic = feed (listify theCompleteAnimation) 
--cartesianPic = feed (listify circleAnimationShort) 


justRectangleTour :: Float -> Picture
justRectangleTour = feed (listify $ rectangleTourShort lin)

justCircleapprox :: Float -> Picture
justCircleapprox = feed (listify ngon2oo)

justCircleapproxShifted :: Float -> Picture
justCircleapproxShifted = feed (listify ngon2ooShifted)

justBounciball :: Float -> Picture
justBounciball = feed (listify bounciball)

justTetris :: Float -> Picture
justTetris = feed (listify tetris)

justBilliardball :: Float -> Picture
justBilliardball = feed (listify billiardBall)

justNiklaushaus :: Float -> Picture
justNiklaushaus = feed (listify $ niklaushaus lin)

animationList :: [Animation]
animationList = listify theCompleteAnimation

theCompleteAnimation :: AnimationShort
theCompleteAnimation = Node [ circleAnimationShort
                            , rectangleTourShort lin
                            , ngon2oo
                            , ngon2ooShifted
                            , bounciball
                            , tetris
                            , billiardBall
                            , niklaushaus lin
                            ]


lin :: Float -> Float -> Float -> Float
lin duration way = ((way/duration) *)

-- not used but kept just in case --                        

----- 3gon scales up, translates up, rotates, 4gon scales up, t u, r,  ... 3-kgon lined up in circle 
----- polar blocks -------
discreteClosedArc from to r = Polygon $ discreteCurve from to r

discreteCurve :: Float -> Float -> Float -> Path
discreteCurve from to r = (0,0): map ((\phi -> (r * cos phi, r * sin phi)) . degToRad)  [from,(from+1)..to]

discreteStrip :: Float -> Float -> Float -> Float -> Picture
discreteStrip from to r0 r1 = Polygon $ discreteCurve from to r1 ++ reverse (discreteCurve from to r0)

firstLayer :: [Picture]
firstLayer = zipWith (\c a -> color c (a 14)) [blue, red, green, black] [discreteClosedArc   0  90
                                                                        ,discreteClosedArc  90 180
                                                                        ,discreteClosedArc 180 270
                                                                        ,discreteClosedArc 270 360]
  
polarBlocks = Node  [Leaf 5.0 (\d t ->                      color blue  $ dA d t)
                    ,Leaf 5.0 (\d t -> Pictures ( rotate  90 ( color red   $ dA d t):take 1 firstLayer))
                    ,Leaf 5.0 (\d t -> Pictures ( rotate 180 ( color green $ dA d t):take 2 firstLayer))
                    ,Leaf 5.0 (\d t -> Pictures ( rotate 270 ( color black $ dA d t):take 3 firstLayer))
                    ,Leaf 5.0 (\d t -> Pictures ( discreteStrip 45 (45 + lin d 90 t) 14 20:firstLayer))
                   ]
              where
                dA d t = discreteClosedArc 0 (lin d 90 t) 14


 
